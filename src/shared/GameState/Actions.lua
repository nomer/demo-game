-- This module stores all action type names and action creators and serves as a
-- single source of truth for such.
local actionTypes = {
	RED_TEAM_SCORED = "RED_TEAM_SCORED",
	BLUE_TEAM_SCORED = "BLUE_TEAM_SCORED",
	RESET_GAME = "RESET_GAME",
	-- This action is used to replicate state to players when they join.
	RUNTIMEINIT = "@@RUNTIMEINIT"
}

local actionCreators = {
	redTeamScored = function()
		return {
			type = actionTypes.RED_TEAM_SCORED
		}
	end,
	blueTeamScored = function()
		return {
			type = actionTypes.BLUE_TEAM_SCORED
		}
	end,
	resetGame = function()
		return {
			type = actionTypes.RESET_GAME
		}
	end,
	-- This action is used to replicate state to players when they join.
	runtimeInit = function(state)
		return {
			type = actionTypes.RUNTIMEINIT,
			state = state
		}
	end
}

return {actionTypes = actionTypes, actionCreators = actionCreators}
