-- This module defines reducers for all actions used in the GameState store.
local Actions = require(script.Parent.Actions)

local redTeamReducer = function(state, action)
	if action.type == Actions.actionTypes.RED_TEAM_SCORED then
		return state + 1
	end
	return state
end

local blueTeamReducer = function(state, action)
	if action.type == Actions.actionTypes.BLUE_TEAM_SCORED then
		return state + 1
	end
	return state
end

local function reducer(state, action)
	if state == nil then
		state = {}
	end

	if action.type == "@@INIT" then
		-- This action is used interally by Rodux to pass initialState.
		return state
	elseif action.type == "@@RUNTIMEINIT" then
		-- This action is used to replicate state to players when they join.
		return action.state
	elseif action.type == Actions.actionTypes.RESET_GAME then
		return {
			redTeam = 0,
			blueTeam = 0
		}
	end

	return {
		redTeam = redTeamReducer(state.redTeam or 0, action),
		blueTeam = blueTeamReducer(state.blueTeam or 0, action)
	}
end

return reducer
