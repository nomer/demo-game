-- This module returns a Rodux Store and uses environment-specific code to
-- create and manage the store.
-- If we want to write tests for this we would either need to use Lemur
-- (for compatibility with action replication), or decouple the store
-- construction and action replication.
local ReplicatedStorage = game:GetService("ReplicatedStorage")
local RunService = game:GetService("RunService")
local Players = game:GetService("Players")

local Rodux = require(ReplicatedStorage.Lib.Rodux)
local Reducer = require(script.Parent.Reducer)
local Actions = require(script.Parent.Actions)

local initialState = {
	redTeam = 0,
	blueTeam = 0
}

-- Normally, environment-specific code in a shared module leads to problems
-- further into development, but this demo uses a very simple shared store model
-- that does not warrant keeping this code in their respective environments.
local store
if RunService:IsServer() then
	-- Due to the completely shared nature of the GameState store, we can
	-- replicate all actions using middleware. We perform replication through
	-- this RemoteEvent:
	local networkedActionRemote = Instance.new("RemoteEvent")
	networkedActionRemote.Name = "NetworkedAction"
	networkedActionRemote.Parent = ReplicatedStorage

	-- Middleware is the most appropriate step to replicate actions in this
	-- case; reducers should remain pure.
	local function middleware(nextDispatch, store)
		return function(action)
			networkedActionRemote:FireAllClients(action)
			print("Replicating action from server")
			nextDispatch(action)
		end
	end

	store = Rodux.Store.new(Reducer, initialState, {middleware})

	-- When a player joins, we need to replicate the GameState to them,
	-- otherwise their local store would become desynced with the server's.
	local function playerAdded(player)
		local currentState = store:getState()
		local action = Actions.actionCreators.runtimeInit(currentState)
		networkedActionRemote:FireClient(player, action)
	end

	Players.PlayerAdded:Connect(playerAdded)

	-- Players sometimes already exist at server script runtime in Play Solo,
	-- so we need to account for this.
	for _, player in pairs(Players:GetPlayers()) do
		playerAdded(player)
	end
else
	store = Rodux.Store.new(Reducer, initialState)

	local networkedActionRemote = ReplicatedStorage:WaitForChild("NetworkedAction")
	networkedActionRemote.OnClientEvent:Connect(function(action)
		print("Received replicated action on client, dispatching...")
		store:dispatch(action)
	end)
end

return store
