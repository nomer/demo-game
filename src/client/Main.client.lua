local ReplicatedStorage = game:GetService("ReplicatedStorage")
local Players = game:GetService("Players")

local GameStore = require(ReplicatedStorage.Source.GameState.Store)
local Roact = require(ReplicatedStorage.Lib.Roact)
local RoactRodux = require(ReplicatedStorage.Lib.RoactRodux)
local View = require(script.Parent.UI.View)

local localPlayer = Players.LocalPlayer
local playerGui = localPlayer.PlayerGui

-- We need to attach the store to the Roact hierarchy so descendants can access
-- it.
local app = Roact.createElement(RoactRodux.StoreProvider, {
	store = GameStore
}, {
	App = Roact.createElement("ScreenGui", {}, {
		View = Roact.createElement(View)
	})
})

Roact.mount(app, playerGui)
