local ReplicatedStorage = game:GetService("ReplicatedStorage")

local Roact = require(ReplicatedStorage.Lib.Roact)

local function Scorekeeper(props)
	return Roact.createFragment({
		RedTeam = Roact.createElement("TextLabel", {
			BackgroundColor3 = Color3.fromRGB(255, 0, 0),
			Size = UDim2.new(0.5, 0, 1, 0),
			TextScaled = true,
			Text = props.redTeamScore,
			TextColor3 = Color3.fromRGB(255, 255, 255),
			TextStrokeTransparency = 0
		}),
		BlueTeam = Roact.createElement("TextLabel", {
			BackgroundColor3 = Color3.fromRGB(0, 0, 255),
			Size = UDim2.new(0.5, 0, 1, 0),
			Position = UDim2.new(0.5, 0, 0, 0),
			TextScaled = true,
			Text = props.blueTeamScore,
			TextColor3 = Color3.fromRGB(255, 255, 255),
			TextStrokeTransparency = 0
		})
	})
end

return Scorekeeper
