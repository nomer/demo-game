local ReplicatedStorage = game:GetService("ReplicatedStorage")

local Roact = require(ReplicatedStorage.Lib.Roact)
local RoactRodux = require(ReplicatedStorage.Lib.RoactRodux)
local Scorekeeper = require(script.Parent.Components.Scorekeeper)

local function View(props)
	return Roact.createElement("Frame", {
		AnchorPoint = Vector2.new(0.5, 0),
		BorderSizePixel = 0,
		Size = UDim2.new(0, 0, 0.1, 0),
		Position = UDim2.new(0.5, 0, 0, 0)
	}, {
		UIAspectRatioConstraint = Roact.createElement("UIAspectRatioConstraint", {
			AspectRatio = 2,
			AspectType = Enum.AspectType.ScaleWithParentSize,
			DominantAxis = Enum.DominantAxis.Height
		}),
		Scorekeeper = Roact.createElement(Scorekeeper, props)
	})
end

-- To keep the Scorekeeper minimal and easily reusable, we pull in state here
-- instead of the component itself.
View = RoactRodux.connect(
	function(state, props)
		return {
			redTeamScore = state.redTeam,
			blueTeamScore = state.blueTeam
		}
	end
)(View)

return View
