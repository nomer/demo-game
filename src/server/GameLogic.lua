local CollectionService = game:GetService("CollectionService")
local Workspace = game:GetService("Workspace")
local ReplicatedStorage = game:GetService("ReplicatedStorage")

-- How many blocks does each team need to collect to win?
local WINNING_SCORE = 10
-- What's the max number of blocks that should be in play?
local MAX_BLOCKS = 5
-- How often should blocks spawn? (in seconds)
local BLOCK_SPAWN_RATE = 2.5

local Actions = require(ReplicatedStorage.Source.GameState.Actions)
local GameStore = require(ReplicatedStorage.Source.GameState.Store)

-- It would be better to use something other than selecting the first result in
-- any other case than a demo.
local redDropoff = CollectionService:GetTagged("RedDropoff")[1]
local blueDropoff = CollectionService:GetTagged("BlueDropoff")[1]
local chute = CollectionService:GetTagged("Chute")[1]
local blockTemplate = CollectionService:GetTagged("BlockTemplate")[1]
local activeBlocks = {}
local isRunning = false
-- runTicket is used to prevent the continuation of a game if it's stopped and
-- started within a short period of time.
local runTicket = 0

local function spawnBlock()
	local block = blockTemplate:Clone()
	block.Anchored = false
	block.Color = Color3.new(math.random(), math.random(), math.random())
	block.CFrame = chute.CFrame
	block.RotVelocity = Vector3.new(math.random(-5, 5), math.random(-5, 5), math.random(-5, 5))
	block.Parent = Workspace
	table.insert(activeBlocks, block)
end

local function touchedDropoff(team)
	return function(part)
		local index
		for i, block in pairs(activeBlocks) do
			if block == part then
				index = i
			end
		end

		if not index then
			return
		end

		table.remove(activeBlocks, index)
		part:Destroy()

		if team == "Red" then
			local action = Actions.actionCreators.redTeamScored()
			GameStore:dispatch(action)
		elseif team == "Blue" then
			local action = Actions.actionCreators.blueTeamScored()
			GameStore:dispatch(action)
		end
	end
end

redDropoff.Touched:Connect(touchedDropoff("Red"))
blueDropoff.Touched:Connect(touchedDropoff("Blue"))

local GameLogic = {}

-- Score checking is done on store change. It can also be done in
-- server-specific reducers but reducers should be kept pure.
local function storeChanged(newState, oldState)
	if newState.redTeam >= WINNING_SCORE then
		GameLogic:DeclareWinner("Red")
	elseif newState.blueTeam >= WINNING_SCORE then
		GameLogic:DeclareWinner("Blue")
	end
end

GameStore.changed:connect(storeChanged)

local function run()
	-- The 'ticket' pattern is effectively a dirty flag implementation. I like
	-- the ticket pattern for its simplicity.
	runTicket = runTicket + 1
	local myRunTicket = runTicket
	while isRunning and runTicket == myRunTicket do
		if #activeBlocks < MAX_BLOCKS then
			spawnBlock()
		end
		wait(BLOCK_SPAWN_RATE)
	end
end

function GameLogic:Start()
	if not isRunning then
		isRunning = true
		local action = Actions.actionCreators.resetGame()
		GameStore:dispatch(action)
		coroutine.wrap(run)()
	end
end

function GameLogic:Stop()
	if isRunning then
		isRunning = false
		-- We don't want to reset the score here in order to let players see
		-- the score before a new round begins.
		runTicket = runTicket + 1
		for i, block in pairs(activeBlocks) do
			block:Destroy()
			activeBlocks[i] = nil
		end
	end
end

function GameLogic:DeclareWinner(teamName)
	print(("%s team won!"):format(teamName))
	GameLogic:Stop()
	delay(5, function()
		GameLogic:Start()
	end)
end

return GameLogic
