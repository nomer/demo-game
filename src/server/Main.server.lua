local ReplicatedStorage = game:GetService("ReplicatedStorage")

local GameStore = require(ReplicatedStorage.Source.GameState.Store)
local Collision = require(script.Parent.Collision)
local GameLogic = require(script.Parent.GameLogic)

Collision:SetupCollisionGroups()
GameLogic:Start()
