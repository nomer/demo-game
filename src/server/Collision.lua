local PhysicsService = game:GetService("PhysicsService")
local Players = game:GetService("Players")
local CollectionService = game:GetService("CollectionService")

local Collision = {}

function Collision:SetupCollisionGroups()
	PhysicsService:CreateCollisionGroup("Players")
	PhysicsService:CreateCollisionGroup("Barrier")
	PhysicsService:CollisionGroupSetCollidable("Players", "Barrier", true)
	PhysicsService:CollisionGroupSetCollidable("Barrier", "Default", false)

	for _, part in ipairs(CollectionService:GetTagged("Barrier")) do
		PhysicsService:SetPartCollisionGroup(part, "Barrier")
	end

	local function characterAdded(character)
		for _, descendant in pairs(character:GetDescendants()) do
			if descendant:IsA("BasePart") then
				PhysicsService:SetPartCollisionGroup(descendant, "Players")
			end
		end
	end

	local function playerAdded(player)
		player.CharacterAdded:Connect(characterAdded)
	end

	Players.PlayerAdded:Connect(playerAdded)

	-- Players sometimes already exist at server script runtime in Play Solo,
	-- so we need to account for this.
	for _, player in pairs(Players:GetPlayers()) do
		playerAdded(player)
	end
end

return Collision
